# Binary Search Tree
Basic implementation of BST (AVL tree)

![screenshot](BinarySearchTree.pdf)

## License
MIT
