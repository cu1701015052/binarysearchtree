public class BinaryNode {

  public int value;
  public BinaryNode left = null;
  public BinaryNode right = null;
  public int height = 0;

  public BinaryNode(int value) {
    this.value = value;
  }
  
  public BinaryNode add(int value) {
    BinaryNode newRoot = this;
    
    if( value <= this.value ) {
        left = addToSubTree(left, value);
        if( heightDifference() == 2 ) {
          if( value <= left.value ) {
            newRoot = rotateRight();
          }else {
            newRoot = rotateLeftRight();
          }
        }
        
    }else {
      
      right = addToSubTree(right, value);
      if( heightDifference() == -2 ) {
        if( value > right.value ) {
          newRoot = rotateLeft();
        }else {
          newRoot = rotateRightLeft();
        }
      }
    }
    newRoot.computeHeight();
    return newRoot;
  }
  
  private BinaryNode addToSubTree(BinaryNode parent, int value) {
    if( parent == null ) {
      return new BinaryNode(value);
    }
    
    parent = parent.add(value);
    return parent;
  }
  
  public void computeHeight() {
    int height = -1;
    if( left != null ) {
      height = left.height;
    }
    
    if( right != null ) {
      height = Math.max(height, right.height);
    }
    
    this.height = height + 1;
  }
  
  public int heightDifference() {
    int leftTarget = 0;
    int rightTarget = 0;
    if( left != null ) {
      leftTarget = 1 + left.height;
    }
    if( right != null ) {
      rightTarget = 1 + right.height;
    }
    return leftTarget - rightTarget;
  }
  
  private BinaryNode rotateRight() {
    System.out.println("Rotate Right on " + Integer.toString(value));

    BinaryNode newRoot = left;
    BinaryNode grandchild = newRoot.right;
    left = grandchild;
    newRoot.right = this;
    computeHeight();
    return newRoot;
  }
  
  private BinaryNode rotateRightLeft() {
    System.out.println("Rotate Right Left on " + Integer.toString(value));

    BinaryNode child = right;
    BinaryNode newRoot = child.left;
    BinaryNode grand1 = newRoot.left;
    BinaryNode grand2 = newRoot.right;
    child.left = grand2;
    right = grand1;
    
    newRoot.left = this;
    newRoot.right = child;
    
    child.computeHeight();
    computeHeight();
    return newRoot;
  }
  
  private BinaryNode rotateLeft() {
    System.out.println("Rotate Left on " + Integer.toString(value));
    BinaryNode newRoot = right;
    BinaryNode grandchild = newRoot.left;
    right = grandchild;
    newRoot.left = this;
    computeHeight();
    return newRoot;
  }
  
  private BinaryNode rotateLeftRight() {
    System.out.println("Rotate Left Right on " + Integer.toString(value));

    BinaryNode child = left;
    BinaryNode newRoot = child.right;
    BinaryNode grand1 = newRoot.left;
    BinaryNode grand2 = newRoot.right;
    child.right = grand1;
    left = grand2;
    newRoot.left = child;
    newRoot.right = this;
    child.computeHeight();
    computeHeight();
    return newRoot;
  }
  
  public BinaryNode removeFormParent(BinaryNode parent, int value) {
    if( parent != null ) {
      return parent.remove(value);
    }
    return null;
  }
  
  public BinaryNode remove(int value) {
    BinaryNode newRoot = this;
    if( value == this.value ) {
      if( left == null ) {
        return right;
      }
      
      BinaryNode child = left;
      while( child.right != null ) {
        child = child.right;
      }
      
      int childKey = child.value;
      left = removeFormParent( left, childKey);
      this.value = childKey;
      
      if( heightDifference() == -2 ) {
        if( right.heightDifference() <= 0 ) {
          newRoot = rotateLeft();
        }else {
          newRoot = rotateRightLeft();
        }
      }
    }else if( value < this.value ) {
      left = removeFormParent(left, value);
      if( heightDifference() == -2 ) {
        if( right.heightDifference() <= 0 ) {
          newRoot = rotateLeft();
        }else {
          newRoot = rotateRightLeft();
        }
      }
    }else {
      right = removeFormParent(right, value);
      if( heightDifference() == -2 ) {
        if( right.heightDifference() >= 0 ) {
          newRoot = rotateRight();
        }else {
          newRoot = rotateLeftRight();
        }
      }
    }
    newRoot.computeHeight();
    return newRoot;
  }
  
  public void print() {
    
    if( left != null ) {
      if( left.left != null || left.right != null ) {
      left.print();
      }
      System.out.print(left.value);
    }else {
      System.out.print("()");
    }
    
    System.out.print("-");
    System.out.print(value);
    System.out.print("-");

    if( right != null ) {
      System.out.print(right.value);
      System.out.print("\n");
      if( right.left != null || right.right != null ) {
      right.print();
      }
    }else {
      System.out.print("()");
      System.out.print("\n");
    }
    

  }
}
