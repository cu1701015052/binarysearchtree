public class BinaryTree {
  public BinaryNode root;
  
  public BinaryTree() {
  }
  
  public void add(int value) {
    if( root != null ) {
      root = root.add(value);
    }else {
      root = new BinaryNode(value);
    }
  }
  
  BinaryNode nodeWithValue(int value) {
    BinaryNode node = root;

    while(node != null) {
      if( node.value == value ) {
        return node;
      }
      
      else if( value < node.value ) {
        node = node.left;

      }else {
        node = node.right;
      }
    }
    return null;
  }
  
  public void print() {
    if( root != null ) {
      root.print();
    }
  }

}
